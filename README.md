Nama : Muhammad Mishbahi Nur Ihsan

NIM : 222011667

Kelas : 3SI3


## Tampilan Utama
![Tampilan 1](1.png)

## Tampilan Input 1
![Tampilan 2](2.png)

## Tampilan Input 2
![Tampilan 3](3.png)

## Tampilan Daftar Mahasiswa
![Tampilan 4](5.png)

## Tampilan Update
![Tampilan 5](6.png)

## Tampilan Setelah Delete
![Tampilan 6](7.png)

## Struktur Project
![Struktur Project](4.png)
